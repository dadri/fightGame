#include "clickablelabel.h"

ClickableLabel::ClickableLabel(QWidget* parent, Qt::WindowFlags f, int number)
    : QLabel(parent), m_number(number)
{

}

ClickableLabel::~ClickableLabel() {}

void ClickableLabel::mousePressEvent(QMouseEvent* event)
{
    emit clicked(m_number);
}

int ClickableLabel::number() const
{
    return m_number;
}
