#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QWidget>
#include <Qt>

class ClickableLabel : public QLabel
{
    Q_OBJECT

public:
    explicit ClickableLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags(), int number = -1);
    ~ClickableLabel();

    int number() const;

signals:
    void clicked(const int number);

protected:
    void mousePressEvent(QMouseEvent* event);

private:
    int m_number;

};

#endif // CLICKABLELABEL_H
