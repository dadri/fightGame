#include "countdown.h"

#include <QTimer>
#include <QFont>

Countdown::Countdown(QColor color)
{
    setPlainText(QString::number(m_countDown));
    setDefaultTextColor(color);
    setFont(QFont("times",80));

    m_timer = new QTimer(this);
    connect(m_timer,&QTimer::timeout,this,&Countdown::decrease);
    m_timer->start(500);
}

Countdown::~Countdown()
{
    delete m_timer;
}

void Countdown::decrease()
{
    m_countDown--;
    if(m_countDown <= 0)
    {
        setPlainText("GO");
        QTimer::singleShot(200, this, [=]()
        {
            emit finishedCounting();
        } );
    }
    else
    {
        setPlainText(QString::number(m_countDown));
    }
}
