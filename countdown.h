#ifndef COUNTDOWN_H
#define COUNTDOWN_H

#include <QGraphicsTextItem>

class Countdown : public QGraphicsTextItem
{
    Q_OBJECT
public:
    Countdown(QColor color);
    ~Countdown();
    int m_countDown = 3;
private:
    QTimer * m_timer;
    void decrease();

signals:
    void finishedCounting();
};

#endif // COUNTDOWN_H
