#include "fireball.h"

#include <QTimer>
#include <QGraphicsView>

#include <QDebug>

Fireball::Fireball(Player *sendingPlayer, int xSpeed, int ySpeed)
{
    // draw graphic
    //Decide on direction
    if(xSpeed==0 && ySpeed<0)   //up
    {
        setPixmap(img->getFireBall().up);
    }
    else if(xSpeed>0 && ySpeed<0)     //rightup
    {
        setPixmap(img->getFireBall().rightUp);
    }
    else if(xSpeed>0 && ySpeed==0)     //right
    {
        setPixmap(img->getFireBall().right);
    }
    else if(xSpeed>0 && ySpeed>0)   //rightdown
    {
        setPixmap(img->getFireBall().rightDown);
    }
    else if (xSpeed==0 && ySpeed>0)
    {
        setPixmap(img->getFireBall().down);
    }
    else if (xSpeed<0 && ySpeed > 0)
    {
        setPixmap(img->getFireBall().leftDown);
    }
    else if (xSpeed<0 && ySpeed == 0)
    {
        setPixmap(img->getFireBall().left);
    }
    else if (xSpeed < 0 && ySpeed < 0)
    {
        setPixmap(img->getFireBall().leftUp);
    }

    m_xSpeed=xSpeed;
    m_ySpeed=ySpeed;
    m_sendingPlayer=sendingPlayer;
}

void Fireball::advance(int phase)
{
    if(phase == 1)
    {
        // get a list of all the items currently colliding with this fireball
        QList<QGraphicsItem *> colliding_items = collidingItems();

        // colliding with player
        QGraphicsItem *item;
        foreach (item, colliding_items)
        {
            if (item != m_sendingPlayer && typeid(*(item)) == typeid(Player))
            {

                // remove from scene
                //scene()->removeItem(colliding_items[i]);
                scene()->removeItem(this);

                // delete from heap
                //delete colliding_items[i];
                //qDebug() << "deleted";
                emit gameover(m_sendingPlayer);
                delete this;
                return;
            }
        }

        //no collision -> move
        setPos(x()+m_xSpeed,y()+m_ySpeed);
        // delete if off screen
        QGraphicsView * view = scene()->views().first();
        if (pos().y() < 0 || pos().y()>view->height() || pos().x() < 0 || pos().x()>view->width())
        {
            //qDebug() << "deleted";
            scene()->removeItem(this);
            delete this;
        }
    }
}
