#ifndef FIREBALL_H
#define FIREBALL_H

#include <QGraphicsPixmapItem>
#include <QObject>

#include "images.h"
#include "player.h"

class Fireball: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Fireball(Player *sendingPlayer=0, int xSpeed = 5, int ySpeed = 0);

private:
    int m_xSpeed;
    int m_ySpeed;
    Images *img = Images::instance();
    Player *m_sendingPlayer;

    void advance(int phase);

signals:
    void gameover(Player * winner);
};

#endif // FIREBALL_H
