#include "game.h"
#include "score.h"
#include "countdown.h"

#include <QDebug>
#include <QKeyEvent>
#include <QTimer>

#include <QLabel>
#include <QMovie>
#include <QGraphicsProxyWidget>

#define GAMESPEED 1000/60
#define GAMESIZEX 1000
#define GAMESIZEY 800
//#define DEBUG

Game::Game(QWidget *parent, BackGround background, KeyList p1KeyList, KeyList p2KeyList) :
    m_parent(parent),/*QGraphicsView(parent),*/ m_backGround(background), m_p1KeyList(p1KeyList), m_p2KeyList(p2KeyList)
{
    setFocusPolicy(Qt::StrongFocus);
    setWindowTitle("FightGame for AP by Adrian Beutler");

    //create Scene
    m_scene = new QGraphicsScene();
    m_scene->setSceneRect(0,0,GAMESIZEX, GAMESIZEY);

    setScene(m_scene);

    QLabel * movieLabel = new QLabel();
    QMovie * backGroundMovie = new QMovie( m_backGround.fileName, QByteArray());
    backGroundMovie->setScaledSize(QSize(GAMESIZEX, GAMESIZEY));
    QGraphicsProxyWidget* proxyWidget = new QGraphicsProxyWidget();
    movieLabel->setAttribute(Qt::WA_NoSystemBackground);
    movieLabel->setMovie(backGroundMovie);
    proxyWidget->setWidget(movieLabel);
    proxyWidget->setPos(0,0);
    movieLabel->show();
    backGroundMovie->start();
    m_scene->addItem(proxyWidget);

    //Settings
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(GAMESIZEX, GAMESIZEY);

    //Add Players
    m_p1 = new Player(Q_NULLPTR, false, m_backGround.p1Color);
    m_p2 = new Player(Q_NULLPTR, true, m_backGround.p2Color);

    resetPlayersPos();

    m_scene->addItem(m_p1);
    m_scene->addItem(m_p2);

    //Add Scores
    m_scoreP1 = new Score(Q_NULLPTR, m_backGround.p1Color);
    m_scene->addItem(m_scoreP1);

    m_scoreP2 = new Score(Q_NULLPTR, m_backGround.p2Color);
    m_scoreP2->setPos(GAMESIZEX-(m_scoreP2->boundingRect().width()+25),0);
    m_scene->addItem(m_scoreP2);

    //FPS
#ifdef DEBUG
    m_fpsText = new QGraphicsTextItem();
    m_fpsText->setOpacity(1);
    m_fpsText->setPos(100,0);
    m_fpsText->setFont(QFont("times",20));
    m_fpsText->setDefaultTextColor(m_backGround.textColor);
    m_scene->addItem(m_fpsText);
#endif

    //ESC Menu
    m_escMenu = new QGraphicsTextItem();
    m_escMenu->setPos(GAMESIZEX/2-350, GAMESIZEY/2-100);
    m_escMenu->setFont(QFont("times",40));
    m_escMenu->setDefaultTextColor(m_backGround.textColor);
    m_escMenu->setPlainText("		GAME PAUSED\n         Press ESC to Resume\nENTER to return to the Start Menu");
    m_scene->addItem(m_escMenu);
    m_escMenu->hide();

    //Connects for win
    connect(m_p1, &Player::gameover, [=]( Player * winner)
    {
        increaseScore(winner);
        resetPlayersPos();

    } );
    connect(m_p2, &Player::gameover, [=]( Player * winner)
    {
        increaseScore(winner);
        resetPlayersPos();
    } );

    //Start Game
    startTimer(GAMESPEED);
    m_fpsTimer.start();
}

Game::~Game()
{
    delete m_scene;
}

void Game::togglePause()
{
    m_isStopped=!m_isStopped;
    if(m_isStopped)
    {
        m_escMenu->show();
    }
    else
    {
        m_escMenu->hide();
    }
}

//! Key Proxy to Players
/**
 * Keys are recognized by the game and stored in the keys map.
 * */
void Game::testCheatCode(QKeyEvent *key)
{
    switch (cheatCode)
    {
    case 0:
        if(key->key()==Qt::Key_S)
        {
            cheatCode++;
        }
        else
        {
            cheatCode=0;
        }
        break;
    case 1:
        if(key->key()==Qt::Key_P)
        {
            cheatCode++;
        }
        else
        {
            cheatCode=0;
        }
        break;
    case 2:
        if(key->key()==Qt::Key_E)
        {
            cheatCode++;
        }
        else
        {
            cheatCode=0;
        }
        break;
    case 3:
        if(key->key()==Qt::Key_Z)
        {
            cheatCode++;
        }
        else
        {
            cheatCode=0;
        }
        break;
    case 4:
        if(key->key()==Qt::Key_I)
        {
            Images *img = Images::instance();
            img->createFireBallPixmaps(true);
        }
        else
        {
            cheatCode=0;
        }
        break;
    default:
        break;
    }
}

void Game::keyPressEvent(QKeyEvent *key)
{
    m_keys[key->key()] = true;
    QWidget::keyPressEvent(key);
    //m_keyPadFlag = key->modifiers() == Qt::KeypadModifier;

    //Non Player Keys
    if(key->key()==Qt::Key_Escape&&!m_cd)
    {
        togglePause();
    }
    if(key->key()==Qt::Key_Return&&m_isStopped&&!m_cd)
    {
        m_parent->show();
        delete this;
    }
    testCheatCode(key);
}

void Game::keyReleaseEvent(QKeyEvent *key)
{
    m_keys[key->key()] = false;
    QWidget::keyReleaseEvent(key);
    //m_keyPadFlag = key->modifiers() == Qt::KeypadModifier;
}

//! Game Loop
/**
 * The Game checks every GAMESPEED what keys are pressed and let the player objects handle the logic
 * */
void Game::timerEvent(QTimerEvent *)
{
#ifdef DEBUG
    int fps = (int)(1/((double)m_fpsTimer.restart()/1000));
    qDebug() << fps;
    m_fpsText->setPlainText(QString::number(fps));
#endif

    if(m_isStopped)
    {
        return;
    }
    //qDebug() << "Restart after    " << m_fpsTimer.restart() << "     milliseconds";

    XDir xFireDirP1 = noDirX;
    YDir yFireDirP1 = noDirY;

    XDir xFireDirP2 = noDirX;
    YDir yFireDirP2 = noDirY;
    //player 1
    if(m_keys[m_p1KeyList.keyJump])
    {
        m_p1->moveUp();
        yFireDirP1 = upY;
    }
    if(m_keys[m_p1KeyList.keyLeft])
    {
        m_p1->moveLeft();
        xFireDirP1 = leftX;
    }
    if(m_keys[m_p1KeyList.keyDown])
    {
        m_p1->moveDown();
        yFireDirP1 = downY;
    }
    if(m_keys[m_p1KeyList.keyRight])
    {
        m_p1->moveRight();
        xFireDirP1 = rightX;
    }
    if(m_keys[m_p1KeyList.keyHit])
    {
        m_p1->hit();
    }
    if(m_keys[m_p1KeyList.keyFireBall])
    {
        m_p1->fireBall(xFireDirP1, yFireDirP1);
    }

    //player 2
    if(m_keys[m_p2KeyList.keyJump])
    {
        m_p2->moveUp();
        yFireDirP2 = upY;
    }
    if(m_keys[m_p2KeyList.keyLeft])
    {
        m_p2->moveLeft();
        xFireDirP2 = leftX;
    }
    if(m_keys[m_p2KeyList.keyDown])
    {
        m_p2->moveDown();
        yFireDirP2 = downY;
    }
    if(m_keys[m_p2KeyList.keyRight])
    {
        m_p2->moveRight();
        xFireDirP2 = rightX;
    }
    if(m_keys[m_p2KeyList.keyHit])
    {
        m_p2->hit();
    }
    if(m_keys[m_p2KeyList.keyFireBall])
    {
        m_p2->fireBall(xFireDirP2, yFireDirP2);
    }

//    //Try to do everything that is planned in this frame
//    m_p1->updatePosition();
//    m_p2->updatePosition();

    m_scene->advance();
    //qDebug() << "One Loop took    " << m_fpsTimer.elapsed() << "     milliseconds";
}

void Game::increaseScore(Player * winner)
{
    if(winner==m_p1)
    {
        m_scoreP1->increase();
    }
    else if(winner==m_p2)
    {
        m_scoreP2->increase();
    }
    else
    {
        qDebug() << "ERROR winner not found";
    }
}

void Game::resetPlayersPos()
{
    m_isStopped = true;
    m_cd = true;
    Countdown *cd = new Countdown(m_backGround.textColor);
    cd->setPos(GAMESIZEX/2-50,GAMESIZEY/2);
    m_scene->addItem(cd);
    connect(cd, &Countdown::finishedCounting, [=]()
    {
        delete cd;
        m_cd = false;
        m_isStopped = false;
    } );

    m_p1->setPos(10,GAMESIZEY/2);
    m_p2->setPos(GAMESIZEX-(50+10), GAMESIZEY/2);
}
