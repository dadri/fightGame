#ifndef GAME_H
#define GAME_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QElapsedTimer>

#include "player.h"
#include "score.h"

struct BackGround
{
    QString fileName;
    QColor p1Color;
    QColor p2Color;
    QColor textColor;
    BackGround(    QString fileName = ":/Resources/bg_water_night.gif",
                   QColor p1Color = Qt::yellow,
                   QColor p2Color = Qt::green,
                   QColor textColor = Qt::yellow)
    {
        this->fileName = fileName;
        this->p1Color = p1Color;
        this->p2Color = p2Color;
        this->textColor = textColor;
    }
};

struct KeyList
{
    Qt::Key keyHit;
    Qt::Key keyFireBall;
    Qt::Key keyJump;
    Qt::Key keyLeft;
    Qt::Key keyDown;
    Qt::Key keyRight;
};

class Game : public QGraphicsView
{
public:
    Game(QWidget * parent=0, BackGround background = BackGround(), KeyList p1KeyList = KeyList(), KeyList p2KeyList = KeyList());
    ~Game();
    void keyPressEvent(QKeyEvent * key);
    void keyReleaseEvent(QKeyEvent *key);

private:
    QWidget *m_parent;

    BackGround m_backGround;
    QGraphicsScene * m_scene;
    Player * m_p1;
    Player * m_p2;
    Score * m_scoreP1;
    Score * m_scoreP2;
    KeyList m_p1KeyList;
    KeyList m_p2KeyList;

    bool m_isStopped = false;
    bool m_cd = false;

    int cheatCode = 0;


    //Keyboard
    QMap<int, bool> m_keys;
    //bool m_keyPadFlag = false;
    void timerEvent(QTimerEvent *);
    void resetPlayersPos();

    //Frames
    QElapsedTimer m_fpsTimer;
    QGraphicsTextItem * m_fpsText;

    //ESC Menu
    QGraphicsTextItem * m_escMenu;
    void togglePause();
    void testCheatCode(QKeyEvent *key);
    void increaseScore(Player *winner);
};

#endif // GAME_H
