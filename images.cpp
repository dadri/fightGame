#include "images.h"
#include <QBitmap>

Images* Images::inst = 0;

void Images::createPlayerPixmaps()
{
    QPixmap runningRight = QPixmap(":/Resources/running.png");
    player.right = runningRight.scaled(QSize(m_sizeX,m_sizeY));
    player.left = player.right.transformed(QTransform().scale(-1, 1));

    QPixmap jumpingRight = QPixmap(":/Resources/jumping.png");
    player.jumpRight = jumpingRight.scaled(QSize(m_sizeX, m_sizeY));
    player.jumpLeft = player.jumpRight.transformed(QTransform().scale(-1, 1));
}

void Images::createFireBallPixmaps(bool cheatActivated)
{
    QPixmap tmpFireball;
    if(cheatActivated)
    {
        tmpFireball = QPixmap(":/Resources/spezi.png");
    }
    else
    {
        tmpFireball = QPixmap(":/Resources/fireball.png");
    }
    fireBall.up = tmpFireball.scaled(QSize(m_sizeX/2, m_sizeY/2));
    fireBall.down = fireBall.up.transformed(QTransform().scale(1, -1));
    fireBall.left = fireBall.up.transformed(QTransform().rotate(-90));
    fireBall.right = fireBall.up.transformed(QTransform().rotate(90));
    fireBall.leftUp = fireBall.up.transformed(QTransform().rotate(-45));
    fireBall.leftDown = fireBall.up.transformed(QTransform().rotate(-135));
    fireBall.rightUp = fireBall.up.transformed(QTransform().rotate(45));
    fireBall.rightDown = fireBall.up.transformed(QTransform().rotate(135));
}

Images::Images()
{
    //Fill Player
    createPlayerPixmaps();

    //Fill Fireball
    createFireBallPixmaps();
}

Images *Images::instance()
{
    if(!inst)
    {
        inst = new Images();
    }
    return inst;
}

int Images::getSizeX() const
{
    return m_sizeX;
}

int Images::getSizeY() const
{
    return m_sizeY;
}

GraphicPlayer Images::getPlayer(QColor color)
{
    GraphicPlayer output = player;
    output.jumpLeft = changeColor(color, player.jumpLeft);
    output.jumpRight = changeColor(color, player.jumpRight);
    output.left = changeColor(color, player.left);
    output.right = changeColor(color, player.right);
    return output;
}

QPixmap Images::changeColor(QColor color, QPixmap input)
{
    QPixmap pxr( input.size() );
    pxr.fill( color );
    pxr.setMask( input.createMaskFromColor( Qt::transparent ) );
    return pxr;
}

GraphicFireball Images::getFireBall() const
{
    return fireBall;
}
