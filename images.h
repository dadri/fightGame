#ifndef IMAGES_H
#define IMAGES_H

#include <QPixmap>

struct GraphicPlayer
{
    QPixmap left;
    QPixmap right;
    QPixmap jumpLeft;
    QPixmap jumpRight;
    //graphic() {}
};

struct GraphicFireball
{
    QPixmap left;
    QPixmap right;

    QPixmap leftUp;
    QPixmap leftDown;
    QPixmap rightUp;
    QPixmap rightDown;

    QPixmap down;
    QPixmap up;
};

class Images
{
public:
    static Images *instance();

    int getSizeX() const;
    int getSizeY() const;

    GraphicPlayer getPlayer(QColor color);
    GraphicFireball getFireBall() const;

    static QPixmap changeColor(QColor color, QPixmap input);

    void createFireBallPixmaps(bool cheatActivated = false);

private:
    static Images *inst;

    Images();

    int m_sizeX = 50;
    int m_sizeY = 50;

    GraphicPlayer player;
    GraphicFireball fireBall;
    void createPlayerPixmaps();
};

#endif // IMAGES_H
