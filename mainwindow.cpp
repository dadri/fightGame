#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "images.h"

#include <QDebug>
#include <QColorDialog>
#include <QVBoxLayout>
#include <clickablelabel.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("FightGame for AP by Adrian Beutler");
    setStatus("TIP: You can change the Background by clicking on the different Pictures");
    setDefault();

    //BackgroundList
    m_backGrounds.append(BackGround(":/Resources/bg_africa.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_airport.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_biker.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_boat.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_dragon.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_field.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_gym.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_harbor.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_house_fire.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_japan.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_leafs.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_mill.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_moon.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_pier.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_plain.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_race.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_road.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_ruin.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_ship.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_temple.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_train.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_tree.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_village_fire.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_village_winter.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_war.gif", Qt::white, Qt::green, Qt::yellow));
    m_backGrounds.append(BackGround(":/Resources/bg_water_night.gif", Qt::white, Qt::green, Qt::yellow));

    QVBoxLayout *boxLayout = new QVBoxLayout;

    for (int i = 0; i < m_backGrounds.size(); ++i)
    {
        ClickableLabel *label = new ClickableLabel(Q_NULLPTR, Qt::WindowFlags(), i);
        connect(label, &ClickableLabel::clicked, [=]( const int number)
        {
            m_selectedBackGround = m_backGrounds.at(number);
            setDefaultColors();
            setStatus("Background was changed to Background "+QString::number(number)+"!");
        } );

        label->setPixmap(m_backGrounds.at(i).fileName);
        boxLayout->addWidget(label);
    }
    ui->scrollAreaWidgetContents->setLayout(boxLayout);

    //connects
    connect(ui->p1ChooseColor, &QPushButton::clicked, [=]( )
    {
        QColor color = QColorDialog::getColor(Qt::yellow, this );
        if( color.isValid() )
        {
            ui->p1Stickman->setPixmap(Images::changeColor(color, *ui->p1Stickman->pixmap()));
            m_selectedBackGround.p1Color=color;
            setStatus("Color of Player 1 was changed");
        }
    } );
    connect(ui->p2ChooseColor, &QPushButton::clicked, [=]( )
    {
        QColor color = QColorDialog::getColor(Qt::yellow, this );
        if( color.isValid() )
        {
            ui->p2Stickman->setPixmap(Images::changeColor(color, *ui->p2Stickman->pixmap()));
            m_selectedBackGround.p2Color=color;
            setStatus("Color of Player 2 was changed");
        }
    } );
    connect(ui->fightButton, &QPushButton::clicked, [=]( )
    {
        Game * game = new Game(this, m_selectedBackGround, getp1Keys(), getp2Keys());
        game->show();
        this->hide();
    } );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setDefaultColors()
{
    ui->p1Stickman->setPixmap(Images::changeColor(m_selectedBackGround.p1Color, *ui->p1Stickman->pixmap()));
    ui->p2Stickman->setPixmap(Images::changeColor(m_selectedBackGround.p2Color, *ui->p2Stickman->pixmap()));
}

void MainWindow::setDefault()
{
    setDefaultColors();

    ui->keySequenceEdit_p1Down->setKeySequence(QKeySequence("s"));
    ui->keySequenceEdit_p1FireBall->setKeySequence(QKeySequence(" "));
    ui->keySequenceEdit_p1Hit->setKeySequence(QKeySequence("q"));
    ui->keySequenceEdit_p1Jump->setKeySequence(QKeySequence("w"));
    ui->keySequenceEdit_p1Left->setKeySequence(QKeySequence("a"));
    ui->keySequenceEdit_p1Right->setKeySequence(QKeySequence("d"));

    ui->keySequenceEdit_p2Down->setKeySequence(QKeySequence("5"));
    ui->keySequenceEdit_p2FireBall->setKeySequence(QKeySequence("0"));
    ui->keySequenceEdit_p2Hit->setKeySequence(QKeySequence("7"));
    ui->keySequenceEdit_p2Jump->setKeySequence(QKeySequence("8"));
    ui->keySequenceEdit_p2Left->setKeySequence(QKeySequence("4"));
    ui->keySequenceEdit_p2Right->setKeySequence(QKeySequence("6"));
}

void MainWindow::setStatus(QString text)
{
    ui->status->setText(text);
}

KeyList MainWindow::getp1Keys()
{
    KeyList output;
    output.keyDown=(Qt::Key)ui->keySequenceEdit_p1Down->keySequence()[0];
    output.keyFireBall=(Qt::Key)ui->keySequenceEdit_p1FireBall->keySequence()[0];
    output.keyHit=(Qt::Key)ui->keySequenceEdit_p1Hit->keySequence()[0];
    output.keyJump=(Qt::Key)ui->keySequenceEdit_p1Jump->keySequence()[0];
    output.keyLeft=(Qt::Key)ui->keySequenceEdit_p1Left->keySequence()[0];
    output.keyRight=(Qt::Key)ui->keySequenceEdit_p1Right->keySequence()[0];
    return output;
}

KeyList MainWindow::getp2Keys()
{
    KeyList output;
    output.keyDown=(Qt::Key)ui->keySequenceEdit_p2Down->keySequence()[0];
    output.keyFireBall=(Qt::Key)ui->keySequenceEdit_p2FireBall->keySequence()[0];
    output.keyHit=(Qt::Key)ui->keySequenceEdit_p2Hit->keySequence()[0];
    output.keyJump=(Qt::Key)ui->keySequenceEdit_p2Jump->keySequence()[0];
    output.keyLeft=(Qt::Key)ui->keySequenceEdit_p2Left->keySequence()[0];
    output.keyRight=(Qt::Key)ui->keySequenceEdit_p2Right->keySequence()[0];
    return output;
}
