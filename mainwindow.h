#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include "game.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void setDefault();
    QList<BackGround> m_backGrounds;
    BackGround m_selectedBackGround;
    void setDefaultColors();
    void setStatus(QString text);
    KeyList getp1Keys();
    KeyList getp2Keys();
};

#endif // MAINWINDOW_H
