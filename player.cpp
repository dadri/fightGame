#include "player.h"
#include "fireball.h"
#include <QGraphicsView>
#include <QTimer>

#include <QDebug>

Player::Player(QGraphicsItem *parent, bool lookLeft, QColor color): QGraphicsPixmapItem(parent)
{
    //Start Graphic
    if(lookLeft)
    {
        m_lookDir = leftX;
    }
    else
    {
        m_lookDir = rightX;
    }
    graphicPlayer = img->getPlayer(color);
}

void Player::moveLeft()
{
    m_xPlaned=-5;
    m_lookDir = leftX;
}

void Player::moveRight()
{
    m_xPlaned=5;
    m_lookDir = rightX;
}

void Player::moveUp()
{
    m_yPlaned=-10;
}

void Player::moveDown()
{
    m_yPlaned=1;
}

void Player::hit()
{
    if(m_lastHit==0)
    {
        m_lastHit = 50;
        m_isHitting = true;
        QTimer::singleShot(200, this, [=]()
        {
            m_isHitting = false;
            //qDebug() << "stopped hitting";
        } );
    }
}

void Player::fireBall(XDir xDir, YDir yDir)
{
    if(m_lastFireball==0)
    {
        //fireball directions
        if(xDir == noDirX && yDir == noDirY) //no direction Key pressed
        {
            xDir = m_lookDir;
        }

        int xPower = 5;
        switch (xDir)
        {
        case leftX:
            xPower = -15;
            break;
        case rightX:
            xPower = 15;
            break;
        case noDirX:
            xPower = 0;
            break;
        default:
            break;
        }

        int yPower = 5;
        switch (yDir)
        {
        case upY:
            yPower = -15;
            break;
        case downY:
            yPower = 15;
            break;
        case noDirY:
            yPower = 0;
            break;
        default:
            break;
        }

        Fireball * fireball = new Fireball(this, xPower, yPower);
        fireball->setPos(x(), y());
        scene()->addItem(fireball);
        connect(fireball, &Fireball::gameover, this, &Player::fireBallGameOver);
        m_lastFireball = 100;
    }
}


void Player::setPlayerPixmap(QPixmap pixmap)
{
    if(!m_isHitting)
    {
        setPixmap(pixmap);
    }
    else
    {
        setPixmap(img->changeColor(Qt::red, pixmap));
    }
}

void Player::advance(int phase)
{
    if(phase == 1)
    {
        //Reset Fireball
        if(m_lastFireball>0)
        {
            m_lastFireball--;
        }
        //Reset Hit
        if(m_lastHit>0)
        {
            m_lastHit--;
        }

        //Touching other player;
        QList<QGraphicsItem *> colliding_items = collidingItems();
        QGraphicsItem *item;
        foreach (item, colliding_items)
        {
            if (typeid(*(item)) == typeid(Player)) //touching other player
            {
                Player *otherPlayer = dynamic_cast<Player*>( item );
                if(m_isHitting) //you are hitting
                {

                    if(!otherPlayer->isHitting())   //other player not hitting
                    {
                        emit gameover(this);
                        return;
                    }
                }
                else if(otherPlayer->isHitting())    //other player is hitting
                {
                    if(!m_isHitting) //you are not hitting
                    {
                        emit gameover(otherPlayer);
                        return;
                    }
                }
                bounceBack(25);
                otherPlayer->bounceBack(25);
            }
        }

        QGraphicsView * view = scene()->views().first();
        int xFinal = x();
        int yFinal = y();

        //x not out of Screen
        if(x() + m_xPlaned + img->getSizeX() <= view->width() && x() + m_xPlaned >= 0)
        {
            xFinal = x()+m_xPlaned;
        }
        //y not out of Screen
        if(y() + m_yPlaned + img->getSizeY() <= view->height() && y() + m_yPlaned >= 0)
        {
            yFinal = y() + m_yPlaned;
        }

        //gravity
        //not on the floor
        if(y()+img->getSizeY() < view->height())
        {
            if(m_lookDir == rightX)
            {
                setPlayerPixmap(graphicPlayer.jumpRight);
            }
            else
            {
                setPlayerPixmap(graphicPlayer.jumpLeft);
            }
            //add downward momentum
            m_yMomentum+=0.20;
            yFinal+=(int) m_yMomentum;
            //grab a wall (when on wall)
            if((xFinal==0 && m_xPlaned<0)|| (xFinal+img->getSizeY() == view->width() && m_xPlaned>0))
            {
                //set downward momentum to 0
                m_yMomentum = 0;
                //dont allow y movement while hanging on a wall
                yFinal=y();
            }
        }
        //hit the floor
        else
        {
            if(m_lookDir == rightX)
            {
                setPlayerPixmap(graphicPlayer.right);
            }
            else
            {
                setPlayerPixmap(graphicPlayer.left);
            }
            //set downward momentum to 0
            m_yMomentum = 0;
        }

        //Fix y pos from overflowing
        if(yFinal+img->getSizeY()>view->height())
        {
            yFinal=view->height()-img->getSizeY();
        }
        setPos(xFinal, yFinal);

        m_xPlaned = 0;
        m_yPlaned = 0;
    }
}

bool Player::isHitting() const
{
    return m_isHitting;
}

void Player::bounceBack(int amount)
{
    if(m_lookDir == rightX)
    {
        m_xPlaned=-amount;
    }
    else
    {
        m_xPlaned=amount;
    }
    //m_yPlaned=-100;
}

void Player::fireBallGameOver(Player *winner)
{
    emit gameover(winner);
}



