#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QGraphicsPixmapItem>

#include "images.h"

enum XDir {leftX, noDirX, rightX};
enum YDir {upY, noDirY, downY};

class Player : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Player(QGraphicsItem * parent=0, bool lookLeft = false, QColor color = Qt::black);

    //Controls
    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
    void hit();
    void fireBall(XDir xDir, YDir yDir);

    bool isHitting() const;
    void bounceBack(int amount);

private:
    int sizeX;
    int SizeY;
    Images *img = Images::instance();

    //Momentum
    double m_xMomentum = 0; //Probably not used because x movement can always be controlled
    double m_yMomentum = 0;

    //Planned direction
    int m_xPlaned = 0;
    int m_yPlaned = 0;

    //Last X dir (aka in what dir are we looking?)
    XDir m_lookDir = rightX;

    //Fireball delay
    int m_lastFireball = 0;

    //Hit (delay)
    int m_lastHit = 0;
    bool m_isHitting = false;

    //player animations
    GraphicPlayer graphicPlayer;

    void setPlayerPixmap(QPixmap pixmap);

    void advance(int phase);

signals:
    void gameover(Player * winner);

public slots:
    void fireBallGameOver(Player * winner);
};

#endif // PLAYER_H
