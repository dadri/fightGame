#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>

class Score: public QGraphicsTextItem
{
public:
    Score(QGraphicsItem * parent=0, QColor color = Qt::black);
    int getScore();

public slots:
    void increase();

private:
    int m_score;
};

#endif // SCORE_H
